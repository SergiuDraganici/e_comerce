package E_Com;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EX_02_01_Enregistrement_d_utilisateur_en_optant_pour_abonnement_Newsletter_nominal {
	ChromeDriver driver;

	@Test
	public void Newsletter() throws InterruptedException {
		// � l'aide du navigateur Chrome, entrez dans la page d'accueil du site.
		driver.get("http://www.tutorialsninja.com/demo/");
		// Trouvez le bouton "My Account" dans le coin sup�rieur droit et cliquez
		// dessus.
		driver.findElement(By.xpath("//span[text()='My Account']")).click();
		Thread.sleep(4000);
		// Pointez le curseur de la souris sur le champ "Register" et cliquez sur
		driver.findElement(By.linkText("Register")).click();
		// Remplir le champ "First Name".
		driver.findElement(By.name("firstname")).sendKeys("Jack");
		// Remplir le champ "Last Name".
		driver.findElement(By.name("lastname")).sendKeys("Daniel");
		// Le champ "E-Mail"rester vide.
		String RandomEmail = "Email" + System.currentTimeMillis() + "@mail.com";
		driver.findElement(By.id("input-email")).sendKeys(RandomEmail);
		// Remplir le champ "Telephone".
		driver.findElement(By.name("telephone")).sendKeys("1234567891");
		// Dans la section "Your Password" remplissez les champs marqu�s d'un "*"
		driver.findElement(By.name("password")).sendKeys("qwerty123");
		// R�p�ter le mot de passe d�fini
		driver.findElement(By.id("input-confirm")).sendKeys("qwerty123");
		// Dans la section "Newsletter", sur la ligne "Subscribe" cliquez sur le bouton
		// radio "Yes�".
		WebElement yesRadioBtn = driver.findElement(By.name("newsletter"));
		yesRadioBtn.click();
		// Cliquez sur les case � cocher "Privacy Policy".
		WebElement agreeChk = driver.findElement(By.name("agree"));
		agreeChk.click();
		// Appuyez sur le bouton "Continue".
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("***avant tous les testes");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@AfterClass
	public void afterClass() {
		System.out.println("***apres tous les testes");
		driver.close();
	}

}