package E_Com;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EX_05_01_Reinitialiser_le_mot_de_passe_oublie {
	ChromeDriver driver;

	@Test
	public void ForgotPassword() throws InterruptedException {
		//� l'aide du navigateur Chrome, entrez dans la page d'accueil du site.
		driver.get("http://www.tutorialsninja.com/demo/");
		//Trouvez le bouton "My Account" dans le coin sup�rieur droit et cliquez dessus.
		driver.findElement(By.xpath("//span[text()='My Account']")).click();
		Thread.sleep(4000);
		//Pointez le curseur de la souris sur le champ "Login" et cliquez sur
		driver.findElement(By.linkText("Login")).click();
		//Dans la sous-fen�tre "Returning Customer", cliquez sur le lien "Forgotten Password"
		driver.findElement(By.linkText("Forgotten Password")).click();
		//Renseignez le champ "E-Mail Address"
		driver.findElement(By.id("input-email")).sendKeys("jackdaniel@gmail.com");
		//Cliquez sur le bouton "Continue"
		driver.findElement(By.xpath("//input[@type='submit']")).click();

	}

	@BeforeClass
	public void beforeClass() {
		System.out.println("***avant tous les testes");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();

	}

	@AfterClass
	public void afterClass() {
		System.out.println("***apres tous les testes");
		driver.close();
	}

}
